# French Crop Usage 
This name encompasses two semantic artefacts: a thesaurus and an ontology. Those artefacts was build and update during two French projects: 
* Vespa: Valeur et optimisation des dispositifs d’épidémiosurveillance dans une stratégie durable de protection des cultures. The thesaurus was first used during the vespa project to index French agricultural alert  bulletins named "Plant Health Bulletins" or "Bulletins de Santé du Végétal" (BSV).
* French ANR D2KAB: Data to Knowledge in Agriculture and Biodiversity (www.d2kab.org). The thesaurus is used during the French ANR D2KAB project to index a new version of the BSV corpora. The thesaurus is also used in other semantic artefact to reference crops. The FCU ontology is used to align FCU thesaurus with other semantic artefact like TAXREF-LD.

## French Crop Usage Thesaurus (FCU)

FrenchCropUsage is a thesaurus of crop organized by usage. 
This thesaurus is dedicated to french agriculture point of view. 
Thus it contains mostly French labels and sometimes English one.

The thesaurus is modelled using skos. 

The file called FrenchCropUsage_****_latest.owl is in owl/xml format. 
The file called FrenchCropUsage_****_latest.rdf is in rdf format that can be used with skosplay.
The file called FrenchCropUsage_****_latest.ttl is in ttl format.
The file called FrenchCropUsage_onto_latest.owl, imports skos, contains the associated ontology fcuo. The instance of skos:ConceptScheme is defined in this file.
The file called FrenchCropUsage_thesaurus_latest.owl, imports FrenchCropUsage_onto_latest.owl, contains the set of skos:Concept that is to say the thesaurus.

## French Crop Usage ontology (FCUO)
This ontology specialized the skos model to create a new class Crop subclass of skos:Concept. fcuo:Crop is a defined class. Thus a reasoner like Hermit should be used to classify all the instances of skos:Concept that belongs to the scheme fcuo:thesaurus_FrenchCropUsage as an instance of Crop. Thus the ontology or the thesaurus could evolved independly. The reasonner will rebuild the links between the class fcuo:Crop and the elements of FCU thesaurus.

The FCUO contains also the object property fcuo:relatedCrop which should be used to link anything to a skos:Concept individual of FCU.

FCUO contains also new objet properties and annotation properties dedicated to FCU alignement with taxonomy like TAXREF-LD.
See the publication 

A new data type property was added to store EPPO Code.

# Access Description and Identifier

The license of FCU and FCUO is https://creativecommons.org/licenses/by/4.0/.

The FCU thesaurus and ontology are published into this git repository, the Agroportal repository, Research Data Gouv repository and a dedicated SPARQL endpoint.

## FCU Thesaurus 
The URI of FCU is https://opendata.inrae.fr/fcu-res/

The FCU thesaurus is available on the agroportal repository https://agroportal.lirmm.fr/ontologies/CROPUSAGE.

The DOI of FCU thesaurus in Research Data Gouv repository is https://doi.org/10.15454/QHFTMX

The SPARQL endpoint for FCU thesaurus is https://rdf.codex.cati.inrae.fr/fcu-res/sparql

## FCU Ontology

The URI of FCUO is https://opendata.inrae.fr/fcu-def/

The FCUO (ontology) is available on the agroportal repository https://agroportal.lirmm.fr/ontologies/FCUO.

The DOI of FCUO is  in Research Data Gouv repository https://doi.org/10.57745/SAANOA

The SPARQL endpoint for FCUO ontology is https://rdf.codex.cati.inrae.fr/fcu-def/sparql

## Git Repository 
The git repository is the reference storage system. It is divided into branches. The master branch contains the last version published on the web. Each old version has a dedicated branch. Note that the branch named enrichissementThesaurus contains files and documentation, used for the semi automatic method proposed in MTSR 2022 paper.

The dev branch is updated by the thesaurus manager. This branch contains the input CSV files, the Cellfie transformation rules and the output OWL-XML file. 

# Sources
This source used to build the different versions of FCU thesaurus are:
* The French Agricultural Dictionary entitled "Larousse agricole" (Larousse has stopped editing this dictionary in 2020). This dictionary provides also some definitions of crops and their explanation of their uses in France. Note that, some tropical crops are not described in this french agricultural dictionary.
* French Wikipedia portal about agriculture (https://fr.wikipedia.org/wiki/Portail:Agriculture_et_agronomie) provides some crop names associated to their explanation of their different uses. Note that the tropical crops are described in this Wikipedia portal.
* The documentation of the annual agricultural statistics called Agreste (https://agreste.agriculture.gouv.fr/agreste-web). 
* The metadata of the graphic register of parcels proposes some crops (https://www.data.gouv.fr/en/datasets/registre-parcellaire-graphique-rpg-contours-des-parcelles-et-ilots-culturaux-et-leur-groupe-de-cultures-majoritaire/).
* The official catalogue of species and varieties of cultivated crops in France produced by the French variety and seed study and control group (GEVES) https://www.geves.fr/catalogue-france/
* Regarding fodder sector, two web sites were studied: The herbe-book web site (https://www.herbe-book.org) and the plantesfourrageres web site (http://www.plantesfourrageres.org/pages/caracteristiques.htm) describe different varieties of fodder crops. Those web sites are published by the French interprofession of seeds and seedling named SEMAE.
* Regarding vegetables there is no crop reference documentation. Thus, the common crops between several data sources were sought: Wikipedia, Bonduelle, FranceAgriMer, Encyclopedia Universalis, the Bec Hellouin organic farm.
* The documentation notice of telepac web service proposes a list of crop names to be used to fill in the parcel description. Telepac service enables farmers to obtain funds from the common agricultural policy https://www1.telepac.agriculture.gouv.fr/telepac/pdf/tas/2019/Dossier-PAC-2019_notice_cultures-precisions.pdf
* The catalog of plant protection products and their uses, fertilizing materials and growing media authorized in France (https://ephy.anses.fr/). The associated database is entitled Ephy. This catalog is published by the French Agency for Food, Environmental and Occupational Health and Safety (ANSES)
* The table Catalogue of Acta published in 2021 was used to translate in english some crop name and to indicate EPPO code. The scientific names provided by this catalogue were store as hidden label. It is the new source used in version 3.2 of FCU.

# Documentation

The gitlab wiki pages contain information about the development of the thesaurus and its modelling choice.

More information are available on the web site http://ontology.irstea.fr/pmwiki.php/Site/FrenchCropUsage.

## Contributors

* Stephan BERNARD, LISC, INRAE (https://orcid.org/0000-0001-9694-1443): he publishes on the web the OWL files of FCU and FCUO and takes care of the associated SPARQL endpoints.
* Florence AMARDEILH, ELZEARD, (https://orcid.org/0000-0002-6306-4437): she validates the conceptualisation of FCUO. For FCU, she proposes some new crops based on existing ressources and validates the vegetable crop descriptions.
* Anna CHEPAIKINA, TSCF, MAIAGE, INRAE (https://orcid.org/0000-0001-7494-7748): she proposes a NLP workflow to extract crop labels from French Alert Bulletins.
* Marine COURTIN, MAIAGE, INRAE (https://orcid.org/0000-0003-4189-4322): she validates the FCU labels by using FCU in NLP workflow.
* Baptiste DARNALA, ELZEARD, LIRMM (https://orcid.org/0000-0001-7390-4850): he reuses FCU concepts in the Plant module of C3PO ontology, that is to say he aligns FCU crops with C3PO Plant description. Thus he validates the vegetable crops of FCU thesaurus. 
* Matthieu HIRSCHY, ACTA (https://orcid.org/0000-0002-2711-7714): he validates some crop descriptions, for example the branch of fruit tree crops. He proposes some new crops and some resources for extracting new crops.
* Juliette RAPHEL, ELZEARD (https://orcid.org/0000-0002-5872-5034): she proposes a new organization of vegetable crops in FCU, proposes new vegetable crops and validates the overall branch of vegetable crops in FCU.
* Catherine ROUSSEY, MISTEA, INRAE (https://orcid.org/0000-0002-3076-5499): she proposes a first draft of conceptualisation for FCUO. She creates the OWL files for FCU and FCUO using Protege tool and cellfie plugin. She updates the description of FCU and FCUO in Agroportal repository.
* Jean-Pierre THEAU, AGIR, INRAE (https://orcid.org/0000-0001-9104-8605): he validates the overall branch of fodder crops and proposes new crops for fodder, meadow and pasture.


## Publications

Part of the FCUO (FCU Ontology) is described in the paper:
* Florence Amardeilh, Sophie Aubin, Stephan Bernard, Catherine Faron, Sonia Bravo, Sonia Bravo, Robert Bossy, Franck Michel, Juliette Raphel, Catherine Roussey. Combining different points of view on plant descriptions: mapping agricultural plant roles and biological taxa. Frontiers in Artificial Intelligence, 2023, Frontiers in Artificial Intelligence, 6, pp.118803. ⟨https://doi.org/10.3389/frai.2023.1188036⟩. ⟨https://hal.science/hal-04289970⟩

The FCU thesaurus is used in several publications:
* B. DARNALA, F. AMARDEILH, C. ROUSSEY, K. TODOROV, C. JONQUET.  C3PO: a crop planning and production process ontology and knowledge graph. Frontiers Artificial Intelligence, Section AI in Food, Agriculture and Water, Research Topic Knowledge Graph Technologies: the Next Frontier of the Food, Agriculture, and Water Domains. Volume 6, 2023, doi.org/10.3389/frai.2023.1187090
* A. CHEPAIKINA, R. BOSSY, C. ROUSSEY, S. BERNARD. Thesaurus Enrichment via Coordination Extraction.  In Proceedings of 16th International Conference on Metadata and Semantics Research (MTSR 2022), London, UK, November 7-11, 2022.  Communications in Computer and Information Science (CCIS), Editors E. Garoufallou and A. Vlachidis, Vol. 1789, pp. XXX. Cham (Switzerland): Springer Nature. (under publication). 
* B. DARNALA, F. AMARDEILH, C. ROUSSEY, K. TODOROV, C. JONQUET. Ontological Representation of Cultivated Plants: Linking Botanical and Agricultural Usages.  In Proceedings of 1st Workshop on Modular Knowledge (MK 2022) at the European Semantic Web Conference (ESWC 2022), Hersonissos, Greece, May 29- June 02, 2022. https://hal.archives-ouvertes.fr/hal-03679652
* C. ROUSSEY, X. DELPUECH, F. AMARDEILH, S. BERNARD, C. JONQUET. Semantic Description of Plant Phenological Development Stages, starting with Grapevine. In Proceedings of 14th international conference on Metadata and Semantics Research Conference (MTSR), Madrid, Spain, 2-4 December 2020.   In: Garoufallou E., Ovalle-Perandones MA. (eds) Metadata and Semantic Research. MTSR 2020. Communications in Computer and Information Science (CCIS), vol 1355. Springer, Cham, p 257-268.  https://dx.doi.org/10.1007/978-3-030-71903-6_25, https://hal.science/hal-02996846.
* C. ROUSSEY, S. BERNARD, F. PINET, X. REBOUD, V. CELLIER, I. SIVADON, D. SIMONNEAU, AL. BOURIGAULT. A methodology for the publication of agricultural alert bulletins as LOD. Computers and Electronics in Agriculture . Volume 142, partie B , novembre 2017, pp. 632-650 . https://dx.doi.org/10.1016/j.compag.2017.10.022, https://hal.inrae.fr/hal-02606598


# Manual Update Method

The RDF file of the FCU thesaurus is updated using Protégé ontology editor v5.1.0 with Cellfie plugin (https://github.com/protegeproject/cellfie-plugin). 
When a new reference source of information is identified, the FCU thesaurus manager creates a new CSV file dedicated to this source. The reference source is then studied to extract crops cultivated in France. If possible, some definitions of the crop and their use description are also identified in the source. The choice of the label name of each crop concept, their definition and their position in the hierarchy is discussed by at least one expert of the agricultural sector. Finally, the thesaurus manager proposes a new reference definition in French in order to justify the position of the crop concept in the thesaurus hierarchy. All the information dedicated to new crop concepts in the source is stored in this CSV file. Each line corresponds to one crop concept. The line contains the labels, the definitions, notes, web link, date of creation, FCU version and parent crops. Cellfie is used to generate individuals of skos:Concept class using transformation rules applied on the CSV file. 
To improve the consistency of the final RDF dataset, we used some SWRL rules to infer all inverse properties using SWRL Protege Tab. 
A final check is performed using the SKOS Play! tool (http://labs.sparna.fr/skos-play): it enables to visualise and control the SKOS model and detects errors.

# Errors

If you found an error in the thesaurus, please post a ticket.
Do not hesitate to contact one of the author: catherine.roussey@inrae.fr.

